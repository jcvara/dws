"use strict";

var next = document.getElementById("next-step"),
    previous = document.getElementById("previous-step"),
    step = document.getElementById("step"),
    zone = document.getElementById("zone");

function previousStep() {
  zone.required = false;
  step.value = "type";
}

function nextStep() {
  step.value = "features";
}

next.addEventListener("click", nextStep, false);
previous.addEventListener("click", previousStep, false);
