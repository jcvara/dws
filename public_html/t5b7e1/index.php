<?php

require('modules/classes.php');

?>
<html>

  <head profile="http://www.w3.org/2005/10/profile">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="Ejercicios Bloque 7">

    <meta name="author" content="Juan Carlos Vara Perez">
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez">

    <link rel="stylesheet" type="text/css" href="../assets/css/t5b7e1.css">

    <link rel="icon" type="image/png" href="../assets/img/favicon-php.png">

    <title>Programacion en PHP</title>

  </head>

  <body>

    <div class="body-wrapper">

      <div class="flex-wrapper">

        <header>

          <div class="header-title">

            <h1>Bloque 7 - Clases</h1>

          </div>

        </header>

        <main>

          <section>

            <div class="exercise">

              <p><h3>Estadisticas</h3></p>

              <?php

                $stat = new Statistic();
                $stat->setSample([rand(1, 100), rand(1, 100), rand(1, 100), rand(1, 100), rand(1, 100), rand(1, 100)]);

                echo '<p>Muestra: ' . $stat->getSample() .  '</p>';
                echo '<p>Elementos: ' . $stat->getNumElements() . '</p>';
                echo '<p>Media: ' . $stat->getAverage() . '</p>';
                echo '<p>Desviacion Estandar: ' . $stat->getStandardDev() . '</p>';

              ?>

            </div>

          </section>

        </main>

        <footer>

          <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
            <img alt="Creative Commons License" src="../assets/img/cc-by-nc-sa.png">
          </a>

          <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="http://about.me/jcvara">Juan Carlos Vara Perez</a>.</p>

        </footer>

      </div>

    </div>

  </body>

</html>
