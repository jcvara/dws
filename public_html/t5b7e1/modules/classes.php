<?php

require('stdev.php');

class Statistic
{

  private $sample = null;

  public function setSample($array)
  {
    $this->sample = $array;
  }

  public function getSample()
  {
    return implode(",",$this->sample);
  }

  public function getNumElements()
  {

    if($this->sample != null) {

      return count($this->sample);

    } else {

      return false;

    }

  }

  public function getAverage()
  {

    if($this->sample != null) {

      return (array_sum($this->sample)/$this->getNumElements());

    } else {

      return false;

    }

  }

  public function getStandardDev()
  {

    if($this->sample != null) {

      return stats_standard_deviation($this->sample);

    } else {

      return false;

    }

  }

}
