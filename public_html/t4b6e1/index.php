<?php

require_once('../helpers/Cookie.php');

$color = 'white';

function create() {

  if(Cookie::put('test', 'Cookie test', $_POST['life'])) {

    return 'Cookie creada...';

  } else {

    return 'Error al crear la cookie...';

  }
};

function check() {

  if(Cookie::exists('test')) {

    return 'Cookie leida...';

  } else {

    return 'No existe la cookie...';

  }
};

function destroy() {

  if(Cookie::delete('test')) {

    return 'Cookie destruida...';

  } else {

    return 'Error al destruir la cookie...';

  }
};

if (isset($_GET['color'])) {

  $color = $_GET['color'];
  Cookie::put('page_color', $color, 3600);

} elseif (Cookie::exists('page_color')) {

  $color = Cookie::get('page_color');

}

if (isset($_POST['action'])) {

  $action = $_POST['action'];
  $cookieMessage = $action();

} else {

  $cookieMessage = 'Seleccione una opcion';

}

?><!DOCTYPE html>
<html>

  <head profile="http://www.w3.org/2005/10/profile">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="Ejercicios Bloque 1">

    <meta name="author" content="Juan Carlos Vara Perez">
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez">

    <link rel="stylesheet" type="text/css" href="../assets/css/t4b6e1.css">

    <link rel="icon" type="image/png" href="../assets/img/favicon-php.png">

    <title>Programacion en PHP</title>

  </head>

  <body>

    <div class="body-wrapper"  id="<?=$color?>">

      <div class="flex-wrapper">

        <header>

          <div class="header-title">

            <h1>Bloque 6 - Cookies</h1>

          </div>

        </header>

        <main>

          <section>

            <div class="cards-wrapper">

              <div class="cards">

                <div class="card">

                  <div class="card-image">
                    <img src="../assets/img/colour-wheel.png" alt="">
                  </div>

                  <div class="card-header">
                    Ejercicio 1
                  </div>

                  <div class="card-copy">
                    <p>Selecciona un color de fondo para la pagina</p>
                  </div>

                  <div class="card-stats">

                    <ul>
                      <li><a href="?color=white">#FFFFFF</a><span>Blanco</span></li>
                      <li><a href="?color=red">#990000</a><span>Rojo</span></li>
                      <li><a href="?color=green">#009900</a><span>Verde</span></li>
                      <li><a href="?color=blue">#000099</a><span>Azul</span></li>
                    </ul>

                  </div>

                </div>

                <div class="card">

                  <div class="card-image">
                    <img src="../assets/img/cookie.png" alt="">
                  </div>

                  <div class="card-header">
                    Ejercicio 2
                  </div>

                  <div class="card-copy">

                    <form action="" method="post" id="cookie-test">

                      <p>Duración de la Cookie <input name="life" value="10" maxlength="2" id="life" type="text"> segundos (1-60)</p>

                      <p><?=$cookieMessage?></p>

                    </form>

                  </div>

                  <div class="card-stats">

                    <ul>
                      <li><button value="create" name="action" type="submit" form="cookie-test">Crear</button></li>
                      <li><button value="check" name="action" type="submit" form="cookie-test">Comprobar</button></li>
                      <li><button value="destroy" name="action" type="submit" form="cookie-test">Destruir</button></li>
                    </ul>

                  </div>

                </div>

                <div class="card">

                  <div class="card-image">
                    <img src="../assets/img/browsers.png" alt="">
                  </div>

                  <div class="card-header">
                    Ejercicio 3
                  </div>

                  <div class="card-copy">
                    <p><?php echo Cookie::put('test', '', -1) ? 'El navegador soporta cookies.' : 'El navegador no soporta cookies.'; ?></p>
                  </div>

                  <div class="card-stats"></div>

                </div>

              </div>

            </div>

          </section>

        </main>

        <footer>

          <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
            <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png">
          </a>

          <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="http://about.me/jcvara">Juan Carlos Vara Perez</a>.</p>

        </footer>

      </div>

    </div>

  </body>

</html>
