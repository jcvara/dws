<?php

class Mysql
{

  private $pdo=null;

  public function getPDO($host, $username, $password, $db)
  {

    if(!$this->pdo)
    {

      if ($db)
      {
        $db = ';dbname=' . $db;
      }

      $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
      );

      try
      {
        $this->pdo = new PDO('mysql:host=' . $host . $db, $username, $password, $options);
      }
      catch (PDOException $e)
      {
        die($e->getMessage());
      }

    }

    return $this->pdo;

  }

}
