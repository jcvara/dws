<?php

require_once(Config::get('path/db') . Config::get('database/type') . '.php');

class Database
{

  private static $instance = null;

  private $pdo,
          $query,
          $error = false,
          $results,
          $count = 0;

  private function __construct()
  {

    $db = ucfirst(Config::get('database/type'));
    $db = new $db;

    $this->pdo = $db->getPDO(
      Config::get('database/host'),
      Config::get('database/username'),
      Config::get('database/password'),
      Config::get('database/db')
    );

  }

  public static function getInstance()
  {

    if (self::$instance === null) {

      self::$instance = new Database();

    }

    return self::$instance;

  }

  public function query($sql, $params = [])
  {

    $this->error = false;

    if ($this->query = $this->pdo->prepare($sql)) {

      $index = 1;

      if (count($params)) {

        foreach ($params as $param) {

          $this->query->bindValue($index, $param);
          $index++;

        }

      }

      if ($this->query->execute()) {

        $this->results = $this->query->fetchAll(PDO::FETCH_OBJ);
        $this->count = $this->query->rowCount();

      } else {

        $this->error = true;

      }

    }

    return $this;

  }

  public function action($action, $table, $where = [])
  {

    if (count($where == 3)) {

      $operators = array('=', '>', '<', '>=', '<=');

      $field = $where[0];
      $operator = $where[1];
      $value = $where[2];

      if (in_array($operator, $operators)) {

        $sql = $action .
               ' FROM ' . $table .
               ' WHERE ' . $field .
               ' ' . $operator . ' ?';

        if (!$this->query($sql, array($value))->error())
        {
          return $this;
        }

      }

    }

    return false;

  }

  public function get($table, $where)
  {

    return $this->action('SELECT *', $table, $where);

  }

  public function insert($table, $fields = [])
  {

    $keys = array_keys($fields);
    $values = '';
    $index = 1;

    foreach ($fields as $field) {

      $values .= '?';

      if ($index < count($fields)) {

        $values .= ', ';

      }

      $index++;

    }

    $sql = 'INSERT INTO ' . $table . ' ("' . implode('", "', $keys) . '") VALUES (' . $values . ')';

    if (!$this->query($sql, $fields)->error()) {

      return true;

    }

    return false;

  }

  public function update($table, $id, $fields)
  {

    $set = '';
    $index = 1;

    foreach ($fields as $name => $value) {

      $set .= $name . ' = ?';

      if ($index < count($fields)) {

        $set .= ', ';

      }

      $index++;

    }

    $sql = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE id = ' . $id;

    if (!$this->query($sql, $fields)->error()) {

      return true;

    }

    return false;

  }

  public function delete($table, $where)
  {
    return $this->action('DELETE', $table, $where);
  }

  public function results()
  {
    return $this->results;
  }

  public function first()
  {
    return $this->results()[0];
  }

  public function count()
  {
    return $this->count;
  }

  public function error()
  {
    return $this->error;
  }

}
