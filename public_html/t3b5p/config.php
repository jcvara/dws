<?php

define('APP_DIR', dirname(__FILE__));
define('BASE_DIR', APP_DIR . '/../');
define('DB_DIR', APP_DIR . '/../helpers/databases/');

define('BASE_URL', 'http://' . $_SERVER['SERVER_NAME']);

final class Config
{

  private static $instance = null;

  private static $configuration = array(
    'database' => array(
      'type' => 'mysql',
      'host' => '127.0.0.1',
      'username' => 'agenda',
      'password' => 'agenda',
      'db' => 'agenda'
    ),
    'remember' => array(
      'cookie_name' => 'hash',
      'cookie_expiry' => 604800
    ),
    'session' => array(
      'session_name' => 'user',
      'token_name' => 'token'
    ),
    'path' => array(
      'app' => APP_DIR,
      'base' => BASE_DIR,
      'db' => DB_DIR,
      'url' => BASE_URL
    )
  );

  public static function getInstance()
  {

    if (self::$instance === null) {

      self::$instance = new Config();

    }

    return self::$instance;

  }

  public function get($path = null)
  {

    if($path) {

      $config = self::$configuration;
      $path = explode('/', $path);

      foreach ($path as $bit) {

        if(isset($config[$bit])) {

          $config = $config[$bit];

        }

      }

      return $config;

    }

    return false;

  }

}
