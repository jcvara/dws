<?php

require_once('config.php');
require_once('../helpers/Database.php');

$num_rec_per_page=10;

if (isset($_POST["page"])) {

	$page  = $_POST["page"];

} else {

	$page=1;

};

?>
<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="description" content="Ejercicios Bloque 1" />

		<meta name="author" content="Juan Carlos Vara Perez" />
		<link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez" />

		<link rel="stylesheet" type="text/css" href="../assets/css/style.css" />

		<title>Agenda</title>

	</head>

	<body>

		<div class="body-wrapper">

			<div class="flex-wrapper">

				<header>

					<div class="header-title">

						<h1>Ejercicio</h1>

					</div>

				</header>

				<main>

					<section>

						<p>Paragraph</p>

					</section>

				</main>

				<footer>Footer</footer>

			</div>

		</div>

	</body>

</html>
