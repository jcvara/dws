<?php

require('modules/product.php');
require('modules/temperature.php');

$product = new Product();

$product->set('desc', 'Producto de prueba');
$product->set('id', 1);
$product->set('price', 2.95);
$product->set('quant', 3);

if (isset($_POST['form'])) {

  if ($_POST['form'] == 1) {

  }

  if ($_POST['form'] == 2) {

    $degrees = new Temperature($_POST['degrees']);

  }

}

?>
<html>

  <head profile="http://www.w3.org/2005/10/profile">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="description" content="Ejercicios Bloque 1">

    <meta name="author" content="Juan Carlos Vara Perez">
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez">

    <link rel="stylesheet" type="text/css" href="../assets/css/t5b7e2.css">

    <link rel="icon" type="image/png" href="../assets/img/favicon-php.png">

    <title>Programacion en PHP</title>

  </head>

  <body>

    <div class="body-wrapper">

      <div class="flex-wrapper">

        <header>

          <div class="header-title">

            <h1>Bloque 7 - Clases</h1>

          </div>

        </header>

        <main>

          <section>

            <div class="exercise">

              <p><h3>Producto</h3></p>

              <p><?php echo 'Descripcion: ' . $product->get('desc'); ?></p>

              <p><?php echo 'ID: ' . $product->get('id'); ?></p>

              <p><?php echo 'Precio: ' . $product->get('price') . '€'; ?></p>

              <p><?php echo 'Cantidad: ' . $product->get('quant'); ?></p>

              <p><?php echo 'IVA: ' . $product->getTax() . '€'; ?></p>

            </div>

            <div class="exercise">

              <p><h3>Restaurante</h3></p>

              <div class="form-wrapper">

                <form method="post">

                  <label for="table">Mesa</label>
                  <select name="table" id="table">
                    <option value="1">Mesa 1</option>
                    <option value="2">Mesa 2</option>
                    <option value="3">Mesa 3</option>
                    <option value="4">Mesa 4</option>
                    <option value="5">Mesa 5</option>
                  </select>

                  <label for="article">Pedido</label>
                  <select name="article" id="article">
                    <option value="1">Hamburguesa sencilla</option>
                    <option value="2">Hamburguesa con queso</option>
                    <option value="3">Hamburguesa especial</option>
                    <option value="4">Papas fritas</option>
                    <option value="5">Refresco</option>
                    <option value="6">Postre</option>
                  </select>

                  <label for="quantity">Cantidad</label>
                  <input type="text" name="quantity" class="textbox">

                  <button>Añadir</button>

                  <input type="hidden" name="form" value="1">

                </form>

              </div>

            </div>

            <div class="exercise">

              <p><h3>Temperaturas</h3></p>

              <div class="form-wrapper">

                <form method="post">

                  <label for="degrees">Introduzca una temperatura</label>
                  <input type="text" name="degrees" class="textbox">

                  <button>Enviar</button>

                  <input type="hidden" name="form" value="2">

                </form>

              </div>

              <?php

                if(isset($degrees)) {
                  echo '<p>La temperatura en ºF es: ' . $degrees->getFarenheit() . 'º</p>';
                }

              ?>

            </div>

          </section>

        </main>

        <footer>

          <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
            <img alt="Creative Commons License" src="../assets/img/cc-by-nc-sa.png">
          </a>

          <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="http://about.me/jcvara">Juan Carlos Vara Perez</a>.</p>

        </footer>

      </div>

    </div>

  </body>

</html>
