<?php

class Temperature
{

  private $degrees = null;

  function __construct($degrees = 0)
  {

    $this->degrees = $degrees;

  }

  public function getCelsius()
  {
    return $this->degrees;
  }

  public function getFarenheit()
  {
    return ($this->degrees * 9/5) + 32;
  }

  public function getKelvin()
  {
    return $this->degrees + 273.15;
  }

}
