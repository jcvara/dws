<?php

class Product
{

  private $desc = null;
  private $id = null;
  private $price = null;
  private $quant = null;

  function __construct($desc = '', $id = 0, $price = 0, $quant = 0)
  {

    $this->desc = $desc;
    $this->id = $id;
    $this->price = $price;
    $this->quant = $quant;

  }

  public function get($property)
  {
    return $this->$property;
  }

  public function set($property, $value)
  {
    $this->$property = $value;
  }

  public function getTax()
  {
    return round($this->price * 0.21, 2);
  }

}
