<?php

require('../helpers/Session.php');
require('modules/functions.php');

Session::start('t4b6e2');

$steps = ['type', 'zone', 'features', 'extras', 'results'];
$step = 0;

$data = [];
$sessionData = null;

if (Session::exists('t4b6e2') && !empty($_POST)) {

  $sessionData = json_decode(Session::get('t4b6e2'), true);
  $step = $sessionData['step'];

}

if (isset($_POST['next'])) {$step += 1;}
if (isset($_POST['previous'])) {$step -= 1;}

call_user_func($steps[$step]);

?>
<!DOCTYPE html>
<html>

  <head profile="http://www.w3.org/2005/10/profile">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="Ejercicios Bloque 6">

    <meta name="author" content="Juan Carlos Vara Perez">
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez">

    <link rel="stylesheet" type="text/css" href="../assets/css/t4b6e2.css">

    <link rel="icon" type="image/png" href="../assets/img/favicon-php.png">

    <title>Programacion en PHP</title>

  </head>

  <body>

    <div class="body-wrapper">

      <div class="flex-wrapper">

        <header>

          <div class="header-title">

            <h1>Bloque 6 - Sesiones</h1>

          </div>

        </header>

        <main>

          <div class="breadcrumb">
            <a href="#">Tipo</a>
            <a href="#">Zona</a>
            <a href="#">Caracteristicas</a>
            <a href="#">Extras</a>
          </div>

          <section>

            <div class="wizard">

              <?php

                require('modules/' . $steps[$step] . '.php');

              ?>

            </div>

          </section>

        </main>

        <footer>

          <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
            <img alt="Creative Commons License" src="../assets/img/cc-by-nc-sa.png">
          </a>

          <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="http://about.me/jcvara">Juan Carlos Vara Perez</a>.</p>

        </footer>

      </div>

    </div>

  </body>

</html>
