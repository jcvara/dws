<h3>Paso 4: Elija la zona de la vivienda</h3>

<form method="post">

  <label>Zona: <select name="zone" id="zone">
      <option value="">Seleccionar...</option>
      <option value="1">Centro</option>
      <option value="2">Periferia</option>
      <option value="3">Urbanizacion</option>
    </select>
  </label>

  <button type="submit" class="previous" id="previous-step">&lt; Anterior</button>
  <button type="submit" class="next" id="next-step">Siguiente &gt;</button>

</form>
