<h3>Paso 3: Elija las caracteristicas basicas de la vivienda</h3>

<form method="post">

  <p>Dormitorios: <label><input type="radio" name="rooms" value="1" checked> 1</label>
    <label><input type="radio" name="rooms" value="2"> 2</label>
    <label><input type="radio" name="rooms" value="3"> 3</label>
    <label><input type="radio" name="rooms" value="4"> 4</label>
    <label><input type="radio" name="rooms" value="5"> 5</label>
  </p>

  <p>Precio (€): <label><input type="radio" name="price" value="1" checked> <100.000</label>
    <label><input type="radio" name="price" value="2"> 100.000-200.000</label>
    <label><input type="radio" name="price" value="3"> 200.000-300.000</label>
    <label><input type="radio" name="price" value="4"> >300.000</label>
  </p>

  <button type="submit" class="previous" name="previous">< Anterior</button>
  <button type="submit" class="next" name="next">Siguiente ></button>

</form>
