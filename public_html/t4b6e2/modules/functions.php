<?php

function type()
{

  if (Session::exists('t4b6e2')) {
    Session::delete('t4b6e2');
  }

}

function zone()
{

  if (!empty($_POST)) {

    $data['step'] = 1;
    $data['type'] = $_POST['type'];

    Session::put('t4b6e2', json_encode($data));

  } else {

    $step = 0;
    Session::delete('t4b6e2');

  }

}

function features()
{

  if (Session::exists('t4b6e2') && !empty($_POST)) {

    $data = json_decode(Session::get('t4b6e2'), true);

    $data['step'] = 2;
    $data['zone'] = $_POST['zone'];

    Session::put('t4b6e2', json_encode($data));

  } else {

    $step = 0;
    Session::delete('t4b6e2');

  }

}

function extras()
{

  if (Session::exists('t4b6e2') && !empty($_POST)) {

    $data = json_decode(Session::get('t4b6e2'), true);

    $data['step'] = 3;
    $data['rooms'] = $_POST['rooms'];
    $data['price'] = $_POST['price'];

    Session::put('t4b6e2', json_encode($data));

  } else {

    $step = 0;
    Session::delete('t4b6e2');

  }

}

function results()
{

  if (Session::exists('t4b6e2') && !empty($_POST)) {

    $data = json_decode(Session::get('t4b6e2'), true);

    $data['step'] = 4;
    $data['pool'] = $_POST['pool'];
    $data['lawn'] = $_POST['lawn'];
    $data['garage'] = $_POST['garage'];

    Session::put('t4b6e2', json_encode($data));

  } else {

    $step = 0;
    Session::delete('t4b6e2');

  }

}
