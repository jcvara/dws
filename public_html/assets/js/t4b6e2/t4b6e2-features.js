"use strict";

var next = document.getElementById("next-step"),
    previous = document.getElementById("previous-step"),
    step = document.getElementById("step");

function previousStep() {
  step.value = "zone";
}

function nextStep() {
  step.value = "extras";
}

next.addEventListener("click", nextStep, false);
previous.addEventListener("click", previousStep, false);
