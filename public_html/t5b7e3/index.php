<?php

require '../helpers/Cookie.php';
require 'modules/article.php';
require 'modules/computer.php';
require 'modules/hifi.php';
require 'modules/storage.php';
require 'modules/television.php';
require 'init.php';

$storage = new Storage();
$showCart = false;

if (Cookie::exists('t5b7e3')) {

  $cart = json_decode(Cookie::get('t5b7e3'), true);

  foreach ($cart as $key => $value) {
    $storage->add($items[$key], $value);
  }

}

if (isset($_POST['form'])) {

  if ($_POST['form'] == 1) {

    $storage->add($items[$_POST['product']], $_POST['quantity']);
    $stock = $storage->getStock();
    $sessionData = [];

    foreach ($stock as $item) {
      $sessionData[$item->get('id')] = $item->get('quantity');
    }

    Cookie::put('t5b7e3', json_encode($sessionData), 604800);

  }

  if ($_POST['form'] == 2) {
    $showCart = true;
  }

}

?>
<!doctype html>
<html>

  <head profile="http://www.w3.org/2005/10/profile">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="description" content="Ejercicios Bloque 1">

    <meta name="author" content="Juan Carlos Vara Perez">
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez">

    <link rel="stylesheet" type="text/css" href="../assets/css/t5b7e2.css">

    <link rel="icon" type="image/png" href="../assets/img/favicon-php.png">

    <title>Programacion en PHP</title>

  </head>

  <body>

    <div class="body-wrapper">

      <div class="flex-wrapper">

        <header>

          <div class="header-title">

            <h1>Bloque 7 - Clases</h1>

          </div>

        </header>

        <main>

          <section>

            <div class="exercise">

              <h3>Carro de la compra</h3><br>

              <div class="form-wrapper">

                <form method="post">

                  <label for="product">Producto</label>
                  <select name="product" id="product">
                  <?php
                    foreach ($items as $key => $value) {
                      echo '<option value="' . $key . '">' . $value->get('brand') . ' ' . $value->get('model') . '</option>';
                    }
                  ?>
                  </select>

                  <label for="quantity">Cantidad</label>
                  <input type="text" name="quantity" class="textbox">

                  <button>Añadir</button>

                  <input type="hidden" name="form" value="1">

                </form>

                <form method="post">

                  <button>Mostrar Carrito</button>

                  <input type="hidden" name="form" value="2">

                </form>

              </div>

              <?php

                if ($showCart) {
                  echo '<div>';
                  $storage->printTicket();
                  echo '</div>';
                }

              ?>

            </div>

          </section>

        </main>

        <footer>

          <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
            <img alt="Creative Commons License" src="../assets/img/cc-by-nc-sa.png">
          </a>

          <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="http://about.me/jcvara">Juan Carlos Vara Perez</a>.</p>

        </footer>

      </div>

    </div>

  </body>

</html>
