<?php

class Storage
{

  private $stock = [];

  public function getStock()
  {
    return $this->stock;
  }

  public function add($article, $quantity)
  {

    if (count($this->stock) > 0) {

      foreach ($this->stock as $key => $item) {

        if ($item->get('id') == $article->get('id')) {

          $this->stock[$key]->add($quantity);
          return 1;

        }

      }

      $article->set('quantity', $quantity);
      $this->stock[] = $article;
      return 1;

    } else {

      $article->set('quantity', $quantity);
      $this->stock[] = $article;
      return 1;

    }

    return 0;

  }

  public function remove($article, $quantity)
  {

    if (count($this->stock) > 0) {

      foreach ($this->stock as $key => $item) {

        if ($item->get('id') == $article->get('id')) {

          $this->stock[$key]->remove($quantity);

          if ($this->stock[$key]->get('quantity') <= 0) {
            unset($this->stock[$key]);
          }

          return 1;

        }

      }

      return 0;

    }

    return -1;

  }

  public function printStock()
  {

    foreach ($this->stock as $item) {

      $item->printInfo();

    }

  }

  public function printTicket()
  {

    $total = 0;

    foreach ($this->stock as $item) {

      $quantity = $item->get('quantity');
      $price = $item->get('price');

      $total += $quantity * $price;

      $item->printLine();

    }

    echo '<br><p>Total: ' . $total . '€</p>';

  }

}
