<?php

class Article
{

  protected $brand = '';
  protected $model = '';
  protected $id = null;
  protected $price = null;
  protected $quantity = 0;

  function __construct($properties)
  {

    foreach ($properties as $key => $value) {
      $this->$key = $value;
    }

  }

  public function get($property)
  {
    return $this->$property;
  }

  public function set($property, $value)
  {
    $this->$property = $value;
  }

  public function add($value)
  {
    $this->quantity += $value;
  }

  public function remove($value)
  {
    $this->quantity -= $value;
  }

  public function printInfo()
  {

    $article = get_object_vars($this);
    $excluded = ['brand',
                 'model',
                 'price'];

    echo '<h4>' . $this->brand . ' ' . $this->model . '</h4><br>';

    foreach ($article as $property => $value) {

      if (!in_array($property, $excluded))
       {
        echo '<p>' . ucfirst($property) . ': ' . $value . '</p>';
      }

    }

  }

  public function printLine()
  {
    echo '<p>' . $this->quantity . ' ' . $this->brand . ' ' . $this->model . ': ' . ($this->quantity * $this->price) . '€</p>';
  }

}
