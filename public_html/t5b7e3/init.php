<?php

$items = [];

$item = ['brand' => 'Sony',
       'model' => 'Bravia',
       'id' => 'SONBRA01',
       'price' => '1399.95',
       'quantity' => 0,
       'connectors' => '1 x SCART, 3 x HDMI, 1 x VGA, 1 x RCA, 1 x Composite',
       'resolution' => 'Full HD',
       'refresh' => '100Hz',
       'screen' => 'LED with Bravia Engine',
       'size' => '50"'];

$item = new Television($item);

$items[$item->get('id')] = $item;

$item = ['brand' => 'Samsung',
       'model' => 'S9C Smart TV',
       'id' => 'SAMSTV01',
       'price' => '9999.99',
       'quantity' => 0,
       'connectors' => '4 x HDMI, 1 x VGA, 1 x RCA, 1 x Composite',
       'resolution' => 'Full HD',
       'refresh' => '100Hz',
       'screen' => 'OLED TruColor',
       'size' => '54.6"'];

$item = new Television($item);

$items[$item->get('id')] = $item;

$item = ['brand' => 'Yamaha',
       'model' => 'Suenamucho',
       'id' => 'YAMSUE01',
       'price' => '299.49',
       'quantity' => 0,
       'connectors' => '1 x RCA, 1 x Optical, 2 x Jack',
       'power' => '200W'];

$item = new Hifi($item);

$items[$item->get('id')] = $item;

$item = ['brand' => 'HP',
       'model' => 'Pavilion',
       'id' => 'HPPAV01',
       'price' => '1039.97',
       'quantity' => 0,
       'cpu' => 'AMD Phenom II X6 1090T',
       'memory' => '16Gb',
       'gpu' => 'nVidia GTX470 1280Mb',
       'monitor' => 'ASUS 27"',
       'hdd' => '2Tb',
       'dvd' => 'LG DVD-RW 16x/8x'];

$item = new Computer($item);

$items[$item->get('id')] = $item;
