<?php

require('modules/circumference.php');
require('modules/rectangle.php');
require('modules/date.php');

$today = new CustomDate(date('j'), date('n'), date('Y'));

if (isset($_POST['form'])) {

  if ($_POST['form'] == 1) {

    $object = new Circumference();
    $object->setRadius($_POST['radius']);

  }

  if ($_POST['form'] == 2) {

    $wall = new Rectangle();
    $wall->setWidth($_POST['wWidth']);
    $wall->setHeight($_POST['wHeight']);

    $window = new Rectangle();
    $window->setWidth($_POST['vWidth']);
    $window->setHeight($_POST['vHeight']);

    $paintArea = round($wall->getArea(), 4) - round($window->getArea(), 4);

    $timeToPaint = round($paintArea * 10, 4);

  }

  if ($_POST['form'] == 3) {

    $inputDate = strtotime($_POST['date-ex3']);

    $dateToCompare = new CustomDate();
    $dateToCompare->setDate(date('j', $inputDate), date('n', $inputDate), date('Y', $inputDate));

    $dateDifference = date_diff(
      date_create_from_format('d-m-y', $today->getShortDate()),
      date_create_from_format('d-m-y', $dateToCompare->getShortDate()),
      true
    );

  }

}

?>
<html>

  <head profile="http://www.w3.org/2005/10/profile">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="description" content="Ejercicios Bloque 1">

    <meta name="author" content="Juan Carlos Vara Perez">
    <link rel="author" href="https://plus.google.com/u/0/+JuanCarlosVaraPerez">

    <link rel="stylesheet" type="text/css" href="../assets/css/t5b7e2.css">

    <link rel="icon" type="image/png" href="../assets/img/favicon-php.png">

    <title>Programacion en PHP</title>

  </head>

  <body>

    <div class="body-wrapper">

      <div class="flex-wrapper">

        <header>

          <div class="header-title">

            <h1>Bloque 7 - Clases</h1>

          </div>

        </header>

        <main>

          <section>

            <div class="exercise">

              <div class="exercise-left">

                <p><h3>Circunferencias</h3></p>

                <?php

                  $wheel = new Circumference();
                  $wheel->setRadius(10.2);

                  echo '<p>Area de la rueda: ' . $wheel->getArea() .  'cm<sup>2</sup>.</p>';
                  echo '<p>Perimetro de la rueda: ' . $wheel->getPerimeter() .  'cm.</p>';
                  echo '<br>';

                  $coin = new Circumference();
                  $coin->setRadius(1.4);

                  echo '<p>Area de la moneda: ' . $coin->getArea() .  'cm<sup>2</sup></p>';
                  echo '<p>Perimetro de la moneda: ' . $coin->getPerimeter() .  'cm.</p>';

                ?>

              </div>

              <div class="exercise-right">

                <p><h3>Calculo</h3></p>

                <div class="frm-right-ex1">

                  <label class="rdo-label">Objetos</label>

                  <label class="rdo-label"><input form="frm-ex1" type="radio" name="object" value="wheel" checked>Rueda</label>
                  <label class="rdo-label"><input form="frm-ex1" type="radio" name="object" value="coin">Moneda</label>

                </div>

                <form id="frm-ex1" action="" method="post">

                  <label>Radio <input class="textbox" type="text" name="radius" autofocus required></label>

                  <input type="hidden" name="form" value="1">

                  <div class="btn-wrapper">

                    <button name="btn-action" value="area">Calcular area</button>
                    <button name="btn-action" value="perimeter">Calcular perimetro</button>

                  </div>

                </form>

                <?php

                  if(isset($object)) {

                    echo '<p>El ' . ($_POST['btn-action'] == 'area' ? 'area' : 'perimetro') .
                         ' de la ' . ($_POST['object'] == 'wheel' ? 'rueda' : 'moneda') .
                         ' es: ' . ($_POST['btn-action'] == 'area' ? $object->getArea() : $object->getPerimeter()) .'</p>';

                  }

                ?>

              </div>

            </div>

            <div class="exercise">

              <p><h3>Pintando paredes</h3></p>

              <form id="frm-ex2" action="" method="post">

                <label>Altura de la pared <input type="text" class="textbox" name="wHeight" required></label>
                <label>Anchura de la pared <input type="text" class="textbox" name="wWidth" required></label>
                <label>Altura de la ventana <input type="text" class="textbox" name="vHeight" required></label>
                <label>Anchura de la ventana <input type="text" class="textbox" name="vWidth" required></label>

                <input type="hidden" name="form" value="2"><br>

                <button name="btn-action" value="paint">Calcular tiempo</button>

              </form>

              <?php

                if (isset($timeToPaint)) {
                  echo '<p>Se tardaria ' . $timeToPaint . ' minutos en pintar la pared.</p>';
                }

              ?>

            </div>

            <div class="exercise">

              <p><h3>Diferencia de fechas</h3></p>

              <p>Fecha actual: <?=$today->getLongDate();?>.</p>

              <form id="form-ex3" action="" method="post">

                <label>Introduce una fecha <input type="text" class="textbox" name="date-ex3" placeholder="dd-mm-aaaa" required></label>

                <input type="hidden" name="form" value="3"><br>

                <button name="btn-action" value="days">Calcular dias</button>

              </form>

              <?php

                if (isset($dateDifference)) {
                  echo '<p>La diferencia es de ' . $dateDifference->format('%a') . ' dias.</p>';
                }

              ?>

            </div>

          </section>

        </main>

        <footer>

          <a class="license" rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">
            <img alt="Creative Commons License" src="../assets/img/cc-by-nc-sa.png">
          </a>

          <p>Trabajo bajo licencia <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES">Creative Commons BY-NC-SA 3.0 Unported</a> por <a href="http://about.me/jcvara">Juan Carlos Vara Perez</a>.</p>

        </footer>

      </div>

    </div>

  </body>

</html>
