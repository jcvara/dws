<?php

class Circumference
{

  private $radius = null;

  public function setRadius($value)
  {
    $this->radius = $value;
  }

  public function getRadius()
  {
    return $this->radius;
  }

  public function getArea()
  {

    if($this->radius != null) {

      return round(pow($this->radius, 2) * pi(), 4);

    } else {

      return false;

    }

  }

  public function getPerimeter()
  {

    if($this->radius != null) {

      return round($this->radius * 2 * pi(), 4);

    } else {

      return false;

    }

  }

}
