<?php

class Rectangle
{

  private $width = null;
  private $height = null;

  public function setWidth($value)
  {
    $this->width = $value;
  }

  public function getWidth()
  {
    return $this->width;
  }

  public function setHeight($value)
  {
    $this->height = $value;
  }

  public function getHeight()
  {
    return $this->height;
  }

  public function getArea()
  {

    if($this->width != null && $this->height != null) {

      return $this->width * $this->height;

    } else {

      return false;

    }

  }

}
