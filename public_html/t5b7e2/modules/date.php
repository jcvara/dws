<?php

class CustomDate
{

  private $day = null;
  private $month = null;
  private $year = null;

  function __construct ($initDay = 1, $initMonth = 1, $initYear = 1970)
  {

    if ($this->checkDate($initDay, $initMonth, $initYear)) {

      $this->day = $initDay;
      $this->month = $initMonth;
      $this->year = $initYear;

    } else {

      $this->day = 1;
      $this->month = 1;
      $this->year = 1970;

    }

  }

  public function setDate($day, $month, $year)
  {

    if ($this->checkDate($day, $month, $year)) {

      $this->day = $day;
      $this->month = $month;
      $this->year = $year;

    } else {

      $this->day = 1;
      $this->month = 1;
      $this->year = 1970;

    }

  }

  public function getShortDate()
  {
    return strftime('%d-%m-%G', strtotime($this->day . '-' . $this->month . '-' . $this->year));
  }

  public function getLongDate()
  {
    setlocale(LC_TIME, 'es_ES.UTF8');
    return strftime('%e de %B de %G', strtotime($this->day . '-' . $this->month . '-' . $this->year));
  }

  public function checkDate($day, $month, $year)
  {
    return checkdate($month, $day, $year);
  }

}
