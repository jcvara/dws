var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass'),
    styles = require('../config').styles,
    handleErrors = require('../util/handleErrors');


gulp.task('styles', ['style_clean'], function() {
  return gulp.src(styles.src + '*.scss')
  .pipe(sass(styles.settings))
  .on('error', handleErrors)
  .pipe(autoprefixer({
    browsers: ['last 2 version']
  }))
  .pipe(gulp.dest(styles.dest))
  .pipe(notify({
    message: 'Styles task complete'
  }));
});
