var gulp = require('gulp'),
    config = require('../config');

gulp.task('watch', function() {
  gulp.watch(
    [config.scripts.src + '*.js',
    config.scripts.src + '*/*.js'],
    ['scripts']);
  gulp.watch(
  	[config.styles.src + '*.scss',
  	config.styles.src + '*/*.scss'],
  	['styles']
  );
});
