var gulp = require('gulp'),
    del = require('del'),
    config = require('../config');

gulp.task('script_clean', function(cb) {
  del([config.scripts.dest], cb);
});

gulp.task('style_clean', function(cb) {
  del([config.styles.dest], cb);
});
