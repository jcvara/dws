var gulp = require('gulp'),
    filesize = require('gulp-filesize'),
    jshint = require('gulp-jshint'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    scripts = require('../config').scripts,
    handleErrors = require('../util/handleErrors')

gulp.task('scripts', ['script_clean'], function() {
  return gulp.src([scripts.src + '*.js', scripts.src + '*/*.js'])
  .pipe(jshint('.jshintrc'))
  .pipe(jshint.reporter('jshint-stylish'))
  .pipe(jshint.reporter('fail'))
  .on('error', handleErrors)
  .pipe(gulp.dest(scripts.dest))
  .pipe(filesize())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(uglify())
  .pipe(gulp.dest(scripts.dest))
  .pipe(filesize())
  .pipe(notify({
    message: 'Scripts task complete'
  }));
});
