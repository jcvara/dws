var paths = {
    src: 'sources/',
    dest: 'public_html/assets/'
};

module.exports = {
  paths: {
    src: paths.src,
    dest: paths.dest
  },
  scripts: {
    src: paths.src + 'js/',
    dest: paths.dest + 'js/'
  },
  styles: {
    src: paths.src + 'scss/',
    dest: paths.dest + 'css/',
    settings: {
      errLogToConsole: false,
      includePaths: require('node-refills').includePaths,
      style: 'expanded'
    }
  }
};
